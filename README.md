# Shots Web
Shots is a flexible, secure and open source social network written in NodeJS by [AkshitGarg](/AkshitGarg)

How to run this locally?
0. Clone this repo
1. cd into it
2. Run ```npm i``` to install the dependencies
3. Run ```npm start``` to start it