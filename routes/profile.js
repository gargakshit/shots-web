const express = require('express')
const jwt = require('jsonwebtoken')

const User = require('../models/users')
const Post = require('../models/post')

const router = express.Router()

router.get('/:username', (req, res) => {
    const username = req.params.username
    User.findOne({
        username
    }, (err, user) => {
        if (err) {
            res.send("Error")
        } else {
            const xhr = req.xhr

            Post.find({
                username
            }, (err, posts) => {
                if (err) {
                    res.send("Error")
                } else {
                    const xhr = req.xhr

                    res.render('profile', {
                        name: user.name,
                        username: user.username,
                        email: user.email,
                        about: user.about ? user.about : 'No about :)',
                        pfp: user.pfp,
                        posts,
                        xhr
                    })
                }
            })
        }
    })
})

module.exports = router