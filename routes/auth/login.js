const express = require('express')
const User = require('../../models/users')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const router = express.Router()

router.get('/', (req, res) => {
    if (req.cookies.token) {
        res.redirect('/')
    } else {
        res.render('auth/login', {
            err: null,
            msg: null
        })
    }
})

router.post('/', (req, res) => {
    const {
        username,
        password
    } = req.body

    if (!password || !username) {
        res.status(401).render('auth/login', {
            err: "Please fill all the fields",
            msg: null
        })
    } else {

        User.findOne({
            username
        }, (err, user) => {
            if (err) {
                res.status(500).render('auth/login', {
                    err: "User not found",
                    msg: null
                })
            } else {
                if (!user) {
                    res.status(500).render('auth/login', {
                        err: "User not found",
                        msg: null
                    })
                } else {
                    if (bcrypt.compareSync(password, user.password)) {
                        const token = jwt.sign({
                            username: user.username,
                            _id: user._id,
                            email: user.email
                        }, process.env.SECRET || 'somedevsec')
                        res.cookie("token", token, {
                            expires: new Date(Date.now() + 30 * 24 * 60 * 60 * 1000)
                        }).status(200).redirect('/')
                    } else {
                        res.status(401).render('auth/login', {
                            err: "Password is not correct",
                            msg: null
                        })
                    }
                }
            }
        })
    }

})

module.exports = router