const express = require('express')
const jwt = require('jsonwebtoken')
const bcrypt = require('bcryptjs')

const User = require('../../models/users')

const router = express.Router()

router.get('/', (req, res) => {
    if (req.cookies.token) {
        res.redirect('/')
    } else {
        res.render('auth/register', {
            err: null,
            msg: null
        })
    }
})

router.post('/', (req, res) => {
    const {
        username,
        about,
        pfp,
        email,
        name
    } = req.body

    password = bcrypt.hashSync(req.body.password, 10)

    if (!username || !password || !email || !name) {
        res.status(401).render('/auth/register', {
            err: "Please fill all the fields",
            msg: null
        })
    } else {
        User.findOne({
            username: username
        }, (err, userTC0) => {
            User.findOne({
                email: email
            }, (error, userTC1) => {
                if (error || err || userTC0 || userTC1) {
                    res.status(500).render('auth/register', {
                        err: "Such user exists",
                        msg: null
                    })
                } else {
                    const user = new User({
                        name,
                        pfp,
                        password,
                        email,
                        username
                    })

                    user.save((err, user) => {
                        if (err) {
                            res.status(500).render('auth/register', {
                                err: "Unknown Error",
                                msg: null
                            })
                        } else {
                            const token = jwt.sign({
                                username: user.username,
                                _id: user._id,
                                email: user.email
                            }, process.env.SECRET || 'somedevsec')

                            res.cookie("token", token, {
                                expires: new Date(Date.now() + 30 * 24 * 60 * 60 * 1000)
                            }).status(200).redirect('/')
                        }
                    })
                }
            })
        })
    }
})

module.exports = router