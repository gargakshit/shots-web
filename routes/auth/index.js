const loginRoute = require('./login')
const logoutRoute = require('./logout')
const registerRoute = require('./register')

const express = require('express')

const router = express.Router()

router.use('/login', loginRoute)
router.use('/logout', logoutRoute)
router.use('/register', registerRoute)

module.exports = router