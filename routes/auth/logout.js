const express = require('express')

const router = express()

router.post('/', (req, res) => {
    res.clearCookie("token").redirect('/auth/login')
})

module.exports = router