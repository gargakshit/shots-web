const express = require('express')

const Post = require('../models/post')

const router = express.Router()

router.get('/', (req, res) => {
    if (req.cookies.token) {
        const xhr = req.xhr
        res.render('new', {
            xhr
        })
    } else {
        res.redirect('/auth/login')
    }
})

module.exports = router