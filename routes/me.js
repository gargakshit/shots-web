const express = require('express')
const jwt = require('jsonwebtoken')

const User = require('../models/users')
const Post = require('../models/post')

const router = express.Router()

router.get('/', (req, res) => {
    const token = req.cookies.token
    if (token) {
        jwt.verify(token, process.env.SECRET || 'somedevsec', (err, decoded) => {
            if (err) {
                res.redirect('/auth/login')
            } else {
                User.findById(decoded._id, (err, user) => {
                    if (err) {
                        res.redirect('/auth/login')
                    } else {
                        const xhr = req.xhr

                        Post.find({
                            username: decoded.username
                        }, (err, posts) => {
                            if (err) {
                                res.send("Error")
                            } else {
                                const xhr = req.xhr

                                res.render('me', {
                                    name: user.name,
                                    username: user.username,
                                    email: user.email,
                                    about: user.about ? user.about : 'No about :)',
                                    pfp: user.pfp,
                                    posts,
                                    xhr
                                })
                            }
                        })
                    }
                })
            }
        })
    } else {
        res.redirect('/auth/login')
    }
})

module.exports = router