const express = require('express')
const jwt = require('jsonwebtoken')
const multer = require('multer')
const fs = require('fs')

const upload = multer({
    dest: 'user_content/pfp/'
})

const User = require('../models/users')

const router = express.Router()

router.get('/', (req, res) => {
    const token = req.cookies.token
    if (token) {
        jwt.verify(token, process.env.SECRET || 'somedevsec', (err, decoded) => {
            if (err) {
                res.redirect('/auth/login')
            } else {
                User.findById(decoded._id, (err, user) => {
                    if (err) {
                        res.redirect('/auth/login')
                    } else {
                        const xhr = req.xhr

                        res.render('edit', {
                            name: user.name,
                            username: user.username,
                            email: user.email,
                            about: user.about,
                            pfp: user.pfp,
                            xhr
                        })
                    }
                })
            }
        })
    } else {
        res.redirect('/auth/login')
    }
})

router.post('/', upload.single('pfp'), (req, res) => {
    const token = req.cookies.token
    if (token) {
        jwt.verify(token, process.env.SECRET || 'somedevsec', (err, decoded) => {
            if (err) {
                res.redirect('/auth/login')
            } else {
                const xhr = req.xhr

                if (!req.file) {
                    User.findOneAndUpdate({
                        _id: decoded._id
                    }, {
                        about: req.body.about
                    }, (err, user) => {
                        if (err) {
                            res.send("error")
                        } else {
                            res.redirect('/me')
                        }
                    })
                } else {
                    const path = req.file.path
                    const new_path = 'user_content/pfp/' + decoded._id + '.png'

                    try {
                        fs.unlinkSync(new_path)
                    } catch (err) {

                    } finally {
                        fs.rename(path, new_path, (err) => {
                            if (err) {
                                res.send("Error")
                            } else {
                                User.findOneAndUpdate({
                                    _id: decoded._id
                                }, {
                                    about: req.body.about,
                                    pfp: new_path
                                }, (err, user) => {
                                    if (err) {
                                        res.send("error")
                                    } else {
                                        res.redirect('/me')
                                    }
                                })
                            }
                        })
                    }
                }

            }
        })
    } else {
        res.redirect('/auth/login')
    }
})

module.exports = router