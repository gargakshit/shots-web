const authRoute = require('./auth')
const meRoute = require('./me')
const newRoute = require('./new')
const editRoute = require('./edit')
const profileRoute = require('./profile')

const express = require('express')
const jwt = require('jsonwebtoken')

const router = express.Router()

router.use('/auth', authRoute)
router.use('/me', meRoute)
router.use('/new', newRoute)
router.use('/edit', editRoute)
router.use('/u', profileRoute)

router.get('/', (req, res) => {
    const token = req.cookies.token
    if (!token) {
        res.redirect('/auth/login')
    } else {
        res.render('index', {
            xhr: req.xhr
        })
    }
})

router.get('*', (req, res) => {
    const xhr = req.xhr
    res.render('404', {
        xhr
    })
})

module.exports = router