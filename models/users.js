const mongoose = require('mongoose')

const Schema = mongoose.Schema

const Users = new Schema({
    name: String,
    username: String,
    password: String,
    email: String,
    pfp: String,
    about: String,
    fans: [
        String
    ],
    fanOf: [
        String
    ]
})

module.exports = mongoose.model('Users', Users)