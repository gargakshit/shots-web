const mongoose = require('mongoose')
const Schema = mongoose.Schema

const Posts = new Schema({
    username: {
        type: String,
        required: true,
    },
    postType: {
        type: String,
        required: true,
    },
    comment: {
        type: String,
        required: false,
    },
    image: {
        type: String,
        required: false,
    },
    claps: [
        String
    ],
    timestamp: {
        type: Date,
        default: Date.now()
    }
})

module.exports = mongoose.model('Posts', Posts)