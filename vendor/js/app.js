$(function () {
    var load = function (url) {
        $(".progress").show();
        $.get(url).done(function (data) {
            $("#content").html(data);
            $(".progress").hide();
        })
    };

    $(document).ready(function() {
        $(".progress").hide();
    });

    $(document).on('click', 'a', function (e) {
        e.preventDefault();

        const $this = $(this);
        const url = $this.attr("href");
        const title = $this.text();

        history.pushState({
            url: url,
            title: title
        }, title, url);

        document.title = title;

        load(url);
    })

    $(window).on('popstate', function (e) {
        var state = e.originalEvent.state;
        if (state !== null) {
            document.title = state.title;
            load(state.url);
        } else {
            document.title = 'Shots';
            load('/');
        }
    });
});