const express = require('express')
const path = require('path')
const cookieParser = require('cookie-parser')
const mongoose = require('mongoose')

const routes = require('./routes')

const {
    PORT = 3000,
    NODE_ENV = 'development',
    SHOTS_DB_URI
} = process.env

mongoose.connect(SHOTS_DB_URI, {
    useNewUrlParser: true
}).then((db) => {
    console.log('» MongoDB Connected «')
}).catch((err) => {
    console.log(`» MongoDB didn't connect, Error: ${err} «`)
})

const app = express()

app.use(express.json())
app.use(express.urlencoded({ extended: false }))
app.use(cookieParser())
app.use('/vendor', express.static(path.join(__dirname, 'vendor')))
app.use('/user_content', express.static(path.join(__dirname, 'user_content')))
app.set('views', './views')
app.set('view engine', 'ejs')

app.use('/', routes)

app.listen(PORT, (err) => {
    if (err) throw err
    console.log(`» Express is listening on localhost:${PORT} «`)
})
